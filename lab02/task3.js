let calculateButton = document.querySelector('#calculator')
calculateButton.addEventListener('click', function(event)
{
    let objectA = document.querySelector("[name = 'numberA']")
    let objectB = document.querySelector("[name = 'numberB']")
    let objectResult = document.querySelector("[name = 'result']")
    objectResult.value = parseFloat(objectA.value) + parseFloat(objectB.value);
    event.preventDefault();
});