function dateAgo(){
    let someDate = new Date();
    let numberOfDays = document.getElementById('daysAgo').value;
    someDate.setDate(someDate.getDate() - numberOfDays);
    let month = someDate.getMonth() + 1; //Add 1 because January is set to 0 and Dec is 11
    let day = someDate.getDate();
    let year = someDate.getFullYear();
    document.getElementById('result3').innerHTML += day + "/" + month + "/" + year;
}
function weekDay(Date){
    let day;
    let num = Date.getDay();
    if (Date.getDay() === 0) {
        day = "Неділя";
        num = 7;
    }
    if (Date.getDay() === 1)
        day = "Понеділок";

    if (Date.getDay() === 2)
        day = "Вівторок";

    if (Date.getDay() === 3)
        day = "Середа";

    if (Date.getDay() === 4)
        day = "Четверг";

    if (Date.getDay() === 5)
        day = "П'ятниця";

    if (Date.getDay() === 6)
        day = "Субота";
    return {
        dayNumber: num,
        dayName: day
    };
}

function weekDay1(){
    let  d = new Date();
    document.getElementById('result2').innerHTML = "Номер дня: " + weekDay(d).dayNumber + "<br>" + "Назва дня: " + weekDay(d).dayName;
}

function date(){
    let now = new Date();
    let month;
    if (now.getMonth() === 0)
        month = "Січня";
    if (now.getMonth() === 1)
        month = "Лютого";
    if (now.getMonth() === 2)
        month = "Березня";
    if (now.getMonth() === 3)
        month = "Квітня";
    if (now.getMonth() === 4)
        month = "Травня";
    if (now.getMonth() === 5)
        month = "Червня";
    if (now.getMonth() === 6)
        month = "Липня";
    if (now.getMonth() === 7)
        month = "Серпня";
    if (now.getMonth() === 8)
        month = "Вересня";
    if (now.getMonth() === 9)
        month = "Жовтня";
    if (now.getMonth() === 10)
        month = "Листопада";
    if (now.getMonth() === 11)
        month = "Грудня";

    let day;
    if (now.getDay() === 0)
        day = "Неділя";
    if (now.getDay() === 1)
        day = "Понеділок";
    if (now.getDay() === 2)
        day = "Вівторок";
    if (now.getDay() === 3)
        day = "Середа";
    if (now.getDay() === 4)
        day = "Четверг";
    if (now.getDay() === 5)
        day = "П'ятниця";
    if (now.getDay() === 6)
        day = "Субота";
    document.getElementById('result1').innerHTML = "Дата: " + now.getDate() + " " + month
        + " " + now.getFullYear() + " року\n" + "<br>" + "День: " + day + "<br>" +
        "Час: " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
}
function monthDays(){
    let month = document.getElementById('month').value;
    let year  = document.getElementById('year').value;
    let day;
    if (year % 4 === 0 && month === 2)
        day = 29;
    else if (year % 4 !== 0 && month === 2)
        day = 28;
    else if (month === 1 || month === 3 || month === 5 || month === 7 || month === 8
        || month === 10 || month === 12)
        day = 31;
    else day = 30;
    let date = new Date(year,month - 1, day)
    document.getElementById('result4').innerHTML = "Номер дня: " + day + "<br>" + "Назва дня: " + weekDay(date).dayName;
}

function secondsTime(){
    let date = new Date();
    let time1 = date.getSeconds() + date.getMinutes() * 60 + date.getHours() * 3600;
    let time2 = 86400 - time1;
    document.getElementById('result5').innerHTML = "Пройдено " + time1
        + "<br>" + "Залишилось " + time2;
}
function yearInfo(){
    let year = document.getElementById('year2').value;
    let half;
    if (year % 100 >= 50)
        half = 2;
    else half = 1;
    let century = (year - year % 100) / 100 + 1;
    let th = (year - year % 1000) / 1000 + 1;
    document.getElementById('result6').innerHTML += half + ' ' + century + ' ' + th;

}
function  fullYear(){
    let start = new Date(document.getElementById('date1').value);
    let end = new Date(document.getElementById('date2').value);
    let difference = new Date(end - start).getFullYear() - 1970;
    document.getElementById('result7').innerHTML += "Між датами пройшло повних " + difference.toString() + " років" + "<br>";
}
function greenTime(){
    let rise = document.getElementById('time1').value;
    let sunset = document.getElementById('time2').value;
    let minutes1 = +rise.split(":")[0]*60+ +rise.split(":")[1];
    let minutes2 = +sunset.split(":")[0]*60+ +sunset.split(":")[1];
    let a = 720 - (minutes1 + minutes2)/2;
    document.getElementById('result8').innerHTML += "Відхилення: " + a + "хв" + "<br>";

}