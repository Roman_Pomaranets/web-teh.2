document.addEventListener('DOMContentLoaded', function (){
    fetch("https://raw.githubusercontent.com/alexanderkuzmenko/weatherapplication/master/cities.json")
        .then(response => response.json())
        .then(result => {
            for (let i = 0; i < result.length; i++){
                let a = document.createElement('a')
                a.id = result[i].city
                a.classList.add('list-group-item', 'list-group-item-action')
                a.innerText = result[i].city
                document.querySelector('.list-group').append(a)
            }
            document.getElementById('Zhytomyr').classList.add('active')
            GetWeather('Zhytomyr')
        }).catch()
})

document.querySelector('.list-group').addEventListener('click', function (e){
    for (let elem of document.querySelectorAll('.list-group a')){
        elem.classList.remove('active')
    }
    e.target.classList.add('active')
    GetWeather(e.target.id)
})

document.getElementById('Search').onclick = function doSearch(){
    GetWeather(document.getElementById('SearchInput').value)
}
function WeatherHTML(data){
    let Icode = data.weather[0].icon
    document.getElementById('weatherPNG').src = 'http://openweathermap.org/img/wn/' + Icode + '.png'
    document.querySelector('.card-title').innerHTML = 'Weather in ' + data.name
    document.getElementById('Temperature').innerHTML = Math.round(parseFloat(data.main.temp) - 273.15) + '&deg'
    document.getElementById('Humidity').innerHTML = data.main.humidity + '%'
    document.getElementById('Pressure').innerHTML = data.main.pressure + 'hPa'
}

function GetWeather(city){
    fetch('https://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=9f6599f5ffa4cb5d94ceadc473614661')
        .then(function (response){
            return response.json()
        })
        .then(function (data){
            WeatherHTML(data)
        }).catch(function (){
        window.location.href = '404.html'
    })
}
