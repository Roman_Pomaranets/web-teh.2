let statusDisplay = document.getElementById("status");
let worker;
let fromNumber;
let toNumber;
function cancelSearch() {
    worker.terminate();
    worker = null;
    document.getElementById('searchButton').disabled = false;
}

function doSearch() {
    document.getElementById('searchButton').disabled = true;
    worker = new Worker("PrimeWorker.js");

    fromNumber = document.getElementById('from').value;
    toNumber = document.getElementById('to').value;
    if (localStorage['savedNumber'])
    {
        fromNumber = localStorage['savedNumber'];
        toNumber = localStorage['to'];
    }
    localStorage['from'] = fromNumber;
    localStorage['to'] = toNumber;
    worker.onmessage = receivedWorkerMessage;
    worker.postMessage(
        {
            fr: fromNumber,
            t: toNumber
        }
    );
}
let primeList;

function receivedWorkerMessage(event) {
    let primes = event.data;
    if (primes.length > 1){
        localStorage.clear();
        primeList = "";
        for (let i = 0; i < primes.length; i++) {
            primeList += primes[i];
        }
        let primeContainer = document.getElementById("primeContainer");
        primeContainer.innerHTML = primeList;
        localStorage['pList'] = primeList;
        Notification.requestPermission().then(function (){
            let n = new  Notification('Роботу завершено');
        })
        document.getElementById('searchButton').disabled = false;
    }
    else localStorage['savedNumber'] = primes;
}

window.onload = function (){
    if (localStorage['savedNumber'])
    {
        fromNumber = localStorage['savedNumber'];
        toNumber = localStorage['to'];
    }
    primeList = localStorage['pList'];

    if (primeList){
        document.getElementById('primeContainer').innerHTML = primeList;
        document.getElementById('from').value = fromNumber;
        document.getElementById('to').value = toNumber;
    }
}
document.addEventListener('visibilitychange', function (){
    if(document.hidden){
        let timer = setTimeout(function (){
            let backToSite = new Notification('Поверніться на сайт');
        },60000)
    }
})
navigator.geolocation.getCurrentPosition(
    function(position) {
        document.getElementById('status').innerHTML = 'Последний раз вас засекали здесь: ' +
            position.coords.latitude + ", " + position.coords.longitude;
    }
);
