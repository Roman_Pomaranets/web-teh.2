onmessage = function(event) {
    let primes = findPrimes(event.data.fr, event.data.t);
    postMessage(primes);
};

function findPrimes(fromNumber, toNumber) {
    let list = [];
    for (let i = fromNumber; i<=toNumber; i++) {
        if (i > 1) list.push(i);
    }
    let maxDiv = Math.round(Math.sqrt(toNumber));
    let primes = [];
    let start = performance.now();
    for (let i=0; i<list.length; i++) {
        let failed = false;
        for (let j=2; j<=maxDiv; j++) {
            if ((list[i] !== j) && (list[i] % j === 0)) {
                failed = true;
            } else if ((j === maxDiv) && (failed === false)) {
                postMessage(list[i]);
                primes.push(list[i] + ' - ' + (performance.now() - start).toFixed(5) + '\n');
            }
        }
    }
    return primes;
}
